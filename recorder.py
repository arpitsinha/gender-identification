def recordAudio ():
    import wave
    import pyaudio

    chunkSize=1024
    sampleFormat = pyaudio.paInt32
    channel=1
    samplingRate=44100
    seconds=6
    filename= "output.wav"

    p=pyaudio.PyAudio()

    print ("Recording")
    stream=p.open(format=sampleFormat,
              channels=channel, 
              rate=samplingRate,
              frames_per_buffer=chunkSize,
              input=True)

    frames=[]

    for i in range (0, int (samplingRate/chunkSize*seconds)):
        data= stream.read(chunkSize)
        frames.append(data)

    stream.stop_stream()
    stream.close()

    p.terminate()

    print ("Finished recording")

    wf=wave.open(filename, 'wb')
    wf.setnchannels(channel)

    wf.setsampwidth(p.get_sample_size(sampleFormat))
    wf.setframerate(samplingRate)
    wf.writeframes(b''.join(frames))
    wf.close()
