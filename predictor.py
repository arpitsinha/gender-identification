def predict ():
    import pandas as pd
    import numpy as np
    import pickle
    from Testing import csv
    voices=pd.read_csv(csv)
    voices=np.array(voices)
    file=open('decision_trees.sav', 'rb')
    decision_trees=pickle._load(file)
    x=decision_trees.predict(voices)
    return x