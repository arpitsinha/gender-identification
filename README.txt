GENDER IDENTIFICATION THROUGH VOICE ANALYSIS

Developed by the team of:

Arpit Sinha	UE168022
Arpit Ganesan	UE168020
Anmol Jindal	UE168015
Gourav Gosain	UE168037

_________________________________________________________________________________________

Pre-requisites for installation:
	Software Requirements:
	Anaconda for Python 3.7
	Install external library pyaudio in Anaconda
	R 3.6.1 (preferred location for installation as C:/R)

	Hardware Requirments:
	Dual core processor with 1.6 GHz base processing speed per core
	1 GB RAM
	Microphone (in-built or external)
	
There are two ways to execute the run:
	1. GUI driven
	2. Self driven

1. GUI Driven:
		(a) Run Main.exe using python.exe (located in your anaconda folder) 
		(b) Press record
		(c) Recording Window will appear. You will have 5 seconds to record the voice sample.
			Before you speak, make sure no one else speaks as this system has not been tuned to filter voices, only noise
		(d) Press record and speak for 5 seconds
		(e) Console will show message "recording ended".
		(f) Preprocessing will run in the background and might display warning message "NAs found in dataframes".
			Ignore the error message, it just denotes empty spaces as noise has been filtered out before preprocessing.
		(g) Main Window will reappear with predicted gender "Male" or "Female" depicted at bottom

2. Self driven:
		The software variables have been specified in the Testing.py file which contains:
			flag -> Determines if a sample has been recorded (for Main GUI use)
			Rpath -> Determines path of Rscript.exe
			readas -> Determines the type of read operation to be performed
			file -> Determines name of R file preprocessor
			path -> (Don't change it) Path used by preprocess.py
			csv ->	Name of preprocessed csv file

		This describes the use of various python files:
		recorder -> Records voice for 5 seconds
		output.wav -> recorded audio
		preprocess -> runs preprocessor.R
		preprocessor.R -> filters and processes output.wav to generate foo.csv
		Testing -> contains software variables
		TrainTest -> contains support for both Pickling decision trees as well as plotting accuracy graph for a plarticular fold
		predictor -> Uses pickled decision trees for predicting value of processed audio in foo.csv
		voice.csv -> contains sample used for training
		crossvalidator -> used for cross validation for any fold while keeping number of decision trees constant (60)
		
In TrainTest, the functions both have two arguments:
		n -> No. of folds
		ram -> no. of decision trees or maximum no. of decision trees (in case of plotters)		